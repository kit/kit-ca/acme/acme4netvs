package acme4netvs

import (
	"io"
	"log"
	"os"
	"path/filepath"
)

type DebugPrintfFunc func(format string, a ...any)

func MakeDebugFunc(enable bool, writer io.Writer) DebugPrintfFunc {
	var logger = log.New(writer, "acme4netvs ", log.LstdFlags)
	return func(format string, a ...any) {
		if enable {
			logger.Printf(format, a...)
		}
	}
}

func DebugPrintVersion(printfFunc DebugPrintfFunc, version string) {
	executableName, err := os.Executable()
	if err != nil {
		executableName = "unknown_executable_name"
	} else {
		// Executable name might be an absolute file path
		executableName = filepath.Base(executableName)
	}
	printfFunc(FormatExecutionInfo, executableName, version)
}
