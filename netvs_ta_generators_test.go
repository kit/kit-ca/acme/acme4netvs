package acme4netvs

import (
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuildChallengeFQDN(t *testing.T) {
	var (
		testFQDN              = "uni-beispiel.de"
		expectedChallengeFQDN = "_acme-challenge.uni-beispiel.de"
		result                string
	)
	result = buildChallengeFQDN(testFQDN)
	assert.Equalf(t, result, expectedChallengeFQDN, "buildChallengeFQDN function returned '%s', expected '%s'", result, expectedChallengeFQDN)
}

func TestQuoteTXTData(t *testing.T) {
	var (
		testString           = "i am a teststring"
		expectedQuotedString = "\"i am a teststring\""
		result               string
	)
	result = quoteTXTData(testString)
	assert.Equalf(t, result, expectedQuotedString, "quoteTXTData function returned '%s', expected '%s'", result, expectedQuotedString)
}

func TestGetInformation(t *testing.T) {
	var (
		testTransaction       = NewTARequestBody()
		testFQDN              = "uni-beispiel.de"
		testToken             = "testToken"
		expectedTARequestBody = "[{\"name\":\"dns.fqdn.list\",\"old\":{\"value\":\"uni-beispiel.de\"}},{\"name\":\"dns.fqdn.list\",\"old\":{\"value\":\"_acme-challenge.uni-beispiel.de\"}},{\"name\":\"dns.record.list\",\"old\":{\"data\":\"\\\"testToken\\\"\",\"fqdn_list\":[\"_acme-challenge.uni-beispiel.de\"],\"type\":\"TXT\"}}]"
		result                string
	)
	GetInformation(testTransaction, testFQDN, testToken)
	assert.Equalf(t, testTransaction.NumEntries(), 3, "Expected 3 transaction entries, only got %d", testTransaction.NumEntries())
	taReader := testTransaction.AsRequestBody()
	readerBytes, err := io.ReadAll(taReader)
	assert.Nil(t, err)
	result = string(readerBytes)
	assert.Equalf(t, result, expectedTARequestBody, "Got unexpected transaction content: %s", result)
}

func TestCreateMainFQDN(t *testing.T) {
	var (
		testTransaction       = NewTARequestBody()
		testFQDN              = "uni-beispiel.de"
		expectedTARequestBody = "[{\"name\":\"dns.fqdn.create\",\"new\":{\"description\":\"Automatically created by acme4netvs\",\"type\":\"domain\",\"value\":\"uni-beispiel.de\"}}]"
		result                string
	)
	CreateCertificateFQDN(testTransaction, testFQDN)
	if testTransaction.NumEntries() != 1 {
		t.Errorf("Expected 1 transaction entries, only got %d", testTransaction.NumEntries())
	}
	taReader := testTransaction.AsRequestBody()
	readerBytes, err := io.ReadAll(taReader)
	if err != nil {
		t.Error(err)
	}
	result = string(readerBytes)
	if result != expectedTARequestBody {
		t.Errorf("Got unexpected transaction content: %s", result)
	}
}

func TestCreateChallengeFQDN(t *testing.T) {
	var (
		testTransaction       = NewTARequestBody()
		testFQDN              = "uni-beispiel.de"
		expectedTARequestBody = "[{\"name\":\"dns.fqdn.create\",\"new\":{\"description\":\"ACME dns-01 challenge via acme4netvs\",\"type\":\"meta\",\"value\":\"_acme-challenge.uni-beispiel.de\"}}]"
		result                string
	)
	CreateChallengeFQDN(testTransaction, testFQDN)
	assert.Equalf(t, testTransaction.NumEntries(), 1, "Expected 1 transaction entries, only got %d", testTransaction.NumEntries())
	taReader := testTransaction.AsRequestBody()
	readerBytes, err := io.ReadAll(taReader)
	assert.Nil(t, err)
	result = string(readerBytes)
	assert.Equalf(t, result, expectedTARequestBody, "Got unexpected transaction content: %s", result)
}

func TestCreateChallengeRecord(t *testing.T) {
	var (
		testTransaction       = NewTARequestBody()
		testFQDN              = "uni-beispiel.de"
		testToken             = "testToken"
		expectedTARequestBody = "[{\"name\":\"dns.record.create\",\"new\":{\"data\":\"\\\"testToken\\\"\",\"fqdn\":\"_acme-challenge.uni-beispiel.de\",\"type\":\"TXT\"}}]"
		result                string
	)
	CreateChallengeRecord(testTransaction, testFQDN, testToken)
	assert.Equalf(t, testTransaction.NumEntries(), 1, "Expected 1 transaction entries, only got %d", testTransaction.NumEntries())
	taReader := testTransaction.AsRequestBody()
	readerBytes, err := io.ReadAll(taReader)
	assert.Nil(t, err)
	result = string(readerBytes)
	assert.Equalf(t, result, expectedTARequestBody, "Got unexpected transaction content: %s", result)
}

func TestDeleteChallengeRecord(t *testing.T) {
	var (
		testTransaction       = NewTARequestBody()
		testFQDN              = "uni-beispiel.de"
		testToken             = "testToken"
		expectedTARequestBody = "[{\"name\":\"dns.record.delete\",\"old\":{\"data\":\"\\\"testToken\\\"\",\"fqdn\":\"_acme-challenge.uni-beispiel.de\",\"type\":\"TXT\"}}]"
		result                string
	)

	DeleteChallengeRecord(testTransaction, testFQDN, testToken)
	assert.Equalf(t, testTransaction.NumEntries(), 1, "Expected 1 transaction entries, only got %d", testTransaction.NumEntries())
	taReader := testTransaction.AsRequestBody()
	readerBytes, err := io.ReadAll(taReader)
	assert.Nil(t, err)
	result = string(readerBytes)
	assert.Equalf(t, result, expectedTARequestBody, "Got unexpected transaction content: %s", result)
}
