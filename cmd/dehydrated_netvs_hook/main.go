//go:generate goversioninfo -icon=../../res/acme4netvs-icon.ico -arm=true
package main

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/v2"
)

const ConfigName = "dehydrated_netvs_hook"

// goreleaser variables
var (
	version = "unknown"
	commit  = "unknown"
	date    = "unknown"
)

var (
	apiClient   *acme4netvs.NETVSClient
	debugPrintf acme4netvs.DebugPrintfFunc
	rootCmd     = &cobra.Command{
		Use:   "acme4netvs_dehydrated",
		Short: "ACME DNS-01 hook for dehydrated and KIT NETVS",
		Long: `
ACME DNS-01 hook for dehydrated and KIT NETVS.
This is usually called directly by dehydrated.
` + acme4netvs.CommonTextHelp + acme4netvs.CommonTextLatestRelease,
		Version:          fmt.Sprintf("%s build on %s (commit %s)\n", version, date, commit),
		PersistentPreRun: acme4netvs.CheckTimeoutValues,
		PreRun: func(cmd *cobra.Command, _ []string) {
			debugPrintf = acme4netvs.MakeDebugFunc(!viper.GetBool("quiet"), os.Stderr)
			acme4netvs.DebugPrintVersion(debugPrintf, version)
		},
		Run: func(cmd *cobra.Command, args []string) {
			// do nothing, this just accepts unknown arguments
			if len(args) > 0 {
				debugPrintf(acme4netvs.FormatCommandNotImplemented, args[0])
			} else {
				debugPrintf(acme4netvs.FormatMissingCommand)
				//cmd.Help()
				os.Exit(23)
			}
		},
		Args: func(cmd *cobra.Command, args []string) error {
			return nil
		},
	}
	deployChallengeCmd = &cobra.Command{
		Use:   "deploy_challenge [flags] domain-name challenge-token-ignored challenge-contents",
		Short: "Deploy ACME challenge",
		Long: `
deploy_challenge creates the requested ACME DNS-01 TXT record for the given domain.
This is usually called directly by dehydrated.
` + acme4netvs.CommonTextHelp + acme4netvs.CommonTextLatestRelease,
		Example:          `acme4netvs_dehydrated deploy_challenge --quiet example.kit.edu xxx NzczMGY1NmE3MDRhMDVmNWNmYzI3Mjk3ODI3NzQ3ODAgIC0K`,
		Args:             cobra.MatchAll(cobra.ExactArgs(3)),
		PersistentPreRun: acme4netvs.CheckTimeoutValues,
		PreRun: func(cmd *cobra.Command, _ []string) {
			debugPrintf = acme4netvs.MakeDebugFunc(!viper.GetBool("quiet"), os.Stderr)
			acme4netvs.DebugPrintVersion(debugPrintf, version)
		},
		Run: func(cmd *cobra.Command, args []string) {
			apiClient = acme4netvs.BuildLocalNetVSClient(debugPrintf)

			fqdn, token := args[0], args[2]
			// cleanup fqdn
			fqdn = acme4netvs.HandleChallengePrefix(fqdn, !viper.GetBool("keep-acme-prefix"), debugPrintf)

			err := acme4netvs.DeployChallenge(apiClient, fqdn, token, debugPrintf)
			if err != nil {
				log.Fatalf(acme4netvs.FormatChallengeDeployment, fqdn, err)
			}
			if !viper.GetBool("no-dns-wait") {
				acme4netvs.CheckChallengeOnAllNS(token, fqdn, viper.GetDuration("dns-wait-timeout"), viper.GetDuration("dns-wait-between"), viper.GetDuration("dns-wait-after"), debugPrintf)
			}
		},
	}
	cleanChallengeCmd = &cobra.Command{
		Use:   "clean_challenge [flags] domain-name challenge-token-ignored challenge-contents",
		Short: "Cleanup ACME challenge",
		Long: `
clean_challenge removes the TXT record that was created by deploy_challenge.
This is usually called directly by dehydrated.
` + acme4netvs.CommonTextHelp + acme4netvs.CommonTextLatestRelease,
		Example:          `acme4netvs_dehydrated clean_challenge --apitoken 12345.sdfsfcdfcsd… example.kit.edu xxx NzczMGY1NmE3MDRhMDVmNWNmYzI3Mjk3ODI3NzQ3ODAgIC0K`,
		Args:             cobra.MatchAll(cobra.ExactArgs(3)),
		PersistentPreRun: acme4netvs.CheckTimeoutValues,
		PreRun: func(cmd *cobra.Command, _ []string) {
			debugPrintf = acme4netvs.MakeDebugFunc(!viper.GetBool("quiet"), os.Stderr)
			acme4netvs.DebugPrintVersion(debugPrintf, version)
		},
		Run: func(cmd *cobra.Command, args []string) {
			apiClient = acme4netvs.BuildLocalNetVSClient(debugPrintf)

			fqdn, token := args[0], args[2]
			// cleanup fqdn
			fqdn = acme4netvs.HandleChallengePrefix(fqdn, !viper.GetBool("keep-acme-prefix"), debugPrintf)

			err := acme4netvs.CleanChallenge(apiClient, fqdn, token, debugPrintf)
			if err != nil {
				log.Fatalf(acme4netvs.FormatChallengeCleanup, fqdn, err)
			}
			debugPrintf(acme4netvs.FormatChallengeCleanedUp, fqdn, token)
		},
	}
)

// Add all child commands to the root command and sets flags appropriately.
func main() {
	rootCmd.AddCommand(deployChallengeCmd)
	rootCmd.AddCommand(cleanChallengeCmd)
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	acme4netvs.CommonViperSetup(rootCmd, deployChallengeCmd, cleanChallengeCmd)
	acme4netvs.CommonGlobalFlags(ConfigName, rootCmd)
	acme4netvs.CommonDNSWaitFlags(rootCmd)
	acme4netvs.FinalizeViperSetup(rootCmd)
}
