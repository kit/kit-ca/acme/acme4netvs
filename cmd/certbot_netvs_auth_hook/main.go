//go:generate goversioninfo -icon=../../res/acme4netvs-icon.ico -arm=true
package main

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/v2"
)

const ConfigName = "acme4netvs_certbot_auth_hook"

// goreleaser variables
var (
	version = "unknown"
	commit  = "unknown"
	date    = "unknown"
)

var (
	apiClient   *acme4netvs.NETVSClient
	debugPrintf acme4netvs.DebugPrintfFunc
	rootCmd     = &cobra.Command{
		Use:   "acme4netvs_certbot_auth_hook",
		Short: "ACME DNS-01 challenge hook for certbot and KIT NETVS",
		Long: `
acme4netvs_certbot_auth_hook creates the requested ACME DNS-01 TXT record for the given domain.
This is usually called directly by certbot.

acme4netvs_certbot_auth_hook requires two mandatory environment variables:

  CERTBOT_DOMAIN: The domain being authenticated
  CERTBOT_VALIDATION: The validation string
` + acme4netvs.CommonTextHelp + acme4netvs.CommonTextLatestRelease,
		Args:             cobra.NoArgs,
		PersistentPreRun: acme4netvs.CheckTimeoutValues,
		PreRun: func(cmd *cobra.Command, _ []string) {
			debugPrintf = acme4netvs.MakeDebugFunc(!viper.GetBool("quiet"), os.Stdout)
			acme4netvs.DebugPrintVersion(debugPrintf, version)
		},
		Run: func(cmd *cobra.Command, args []string) {
			apiClient = acme4netvs.BuildLocalNetVSClient(debugPrintf)

			viper.MustBindEnv("certbotdomain", "CERTBOT_DOMAIN")
			if !viper.IsSet("certbotdomain") || len(viper.GetString("certbotdomain")) == 0 {
				log.Fatal(acme4netvs.FormatEmptyDomain)
			}
			fqdn := viper.GetString("certbotdomain")
			// cleanup fqdn
			fqdn = acme4netvs.HandleChallengePrefix(fqdn, !viper.GetBool("keep-acme-prefix"), debugPrintf)

			viper.MustBindEnv("certbotvalidation", "CERTBOT_VALIDATION")
			if !viper.IsSet("certbotvalidation") || len(viper.GetString("certbotvalidation")) == 0 {
				log.Fatalf(acme4netvs.FormatEmptyValidation, fqdn)
			}
			token := viper.GetString("certbotvalidation")

			err := acme4netvs.DeployChallenge(apiClient, fqdn, token, debugPrintf)
			if err != nil {
				log.Fatalf(acme4netvs.FormatChallengeDeployment, fqdn, err)
			}
			if !viper.GetBool("no-dns-wait") {
				acme4netvs.CheckChallengeOnAllNS(token, fqdn, viper.GetDuration("dns-wait-timeout"), viper.GetDuration("dns-wait-between"), viper.GetDuration("dns-wait-after"), debugPrintf)
			}
		},
		Version: fmt.Sprintf("%s build on %s (commit %s)\n", version, date, commit),
	}
)

// Add all child commands to the root command and sets flags appropriately.
func main() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	acme4netvs.CommonViperSetup(rootCmd)
	acme4netvs.CommonGlobalFlags(ConfigName, rootCmd)
	acme4netvs.CommonDNSWaitFlags(rootCmd)
	acme4netvs.FinalizeViperSetup(rootCmd)
}
