package acme4netvs

import (
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	DNSWaitTimeoutMin = time.Second * 5
	DNSWaitBetweenMin = time.Second * 1
)

func CheckTimeoutValues(_ *cobra.Command, _ []string) {
	dnsWaitTimeout := viper.GetDuration("dns-wait-timeout")
	dnsWaitBetween := viper.GetDuration("dns-wait-between")

	if dnsWaitTimeout < DNSWaitTimeoutMin {
		log.Fatalf(FormatDurationTooSmall, "dns-wait-timeout", dnsWaitTimeout, DNSWaitTimeoutMin)
	}
	if dnsWaitBetween < DNSWaitBetweenMin {
		log.Fatalf(FormatDurationTooSmall, "dns-wait-between", dnsWaitBetween, DNSWaitBetweenMin)
	}
}

func CommonViperSetup(commands ...*cobra.Command) {
	for _, c := range commands {
		// disable shell completion
		c.CompletionOptions.DisableDefaultCmd = true
		// allow arguments to start with a dash
		c.Flags().SetInterspersed(false)
		c.PersistentFlags().SetInterspersed(false)
	}
}

func CommonGlobalFlags(appName string, rootCmd *cobra.Command) {
	viper.SetConfigName(appName)
	rootCmd.PersistentFlags().SortFlags = false
	// viper.SetConfigType("env")
	rootCmd.PersistentFlags().StringP("config", "c", "", "Path to NETVS config file containing api token [NETDB_CONFIG_FILE, NETVS_CONFIG_FILE, ACME4NETVS_CONFIG]")
	viper.MustBindEnv("config", "NETDB_CONFIG_FILE", "NETVS_CONFIG_FILE")

	rootCmd.PersistentFlags().String("apiversion", DefaultAPIVersion, "NETDB API version (setting this is a last resort; should normally be set in this binary) [NETDB_API_VERSION, NETVS_API_VERSION, ACME4NETVS_APIVERSION]")
	_ = rootCmd.PersistentFlags().MarkHidden("apiversion") // Ignore error explicitly, happens only if the flag does not exist

	viper.MustBindEnv("apiversion", "NETDB_API_VERSION", "NETVS_API_VERSION")
	viper.SetDefault("apiversion", DefaultAPIVersion)

	rootCmd.PersistentFlags().StringP("apitoken", "a", "", "NETDB API token string [NETDB_API_TOKEN, NETVS_API_TOKEN, ACME4NETVS_APITOKEN]")
	viper.MustBindEnv("apitoken", "NETDB_API_TOKEN", "NETVS_API_TOKEN")

	rootCmd.PersistentFlags().StringP("endpoint", "e", "", `Alternative NETVS API endpoint (either "prod|test|devel" or an existing section from netdb_client.ini) [NETDB_ENDPOINT, NETVS_ENDPOINT, ACME4NETVS_ENDPOINT]`)
	viper.MustBindEnv("endpoint", "NETDB_ENDPOINT", "NETVS_ENDPOINT")

	rootCmd.PersistentFlags().BoolP("quiet", "q", false, "Disable verbose output [ACME4NETVS_QUIET, accepts 'true' or '1' as values]")

	rootCmd.PersistentFlags().Bool("keep-acme-prefix", false, "Do not strip a “_acme-challenge.”-prefix from the domain name [ACME4NETVS_KEEP_ACME_PREFIX]")
}

func CommonDNSWaitFlags(rootCmd *cobra.Command) {
	rootCmd.PersistentFlags().Bool("no-dns-wait", false, "Do not wait until challenge is propagated to all DNS servers ([ACME4NETVS_NO_DNS_WAIT, accepts 'true' or '1' as values]")
	rootCmd.PersistentFlags().Bool("no-dns-failure-logging", false, "Do not output unsuccessful checks of DNS servers ([ACME4NETVS_NO_FAILURE_LOGGING, accepts 'true' or '1' as values]")
	rootCmd.PersistentFlags().Duration("dns-wait-timeout", time.Hour*12, "Time after which the DNS check will fail automatically [ACME4NETVS_DNS_WAIT_TIMEOUT]")
	rootCmd.PersistentFlags().Duration("dns-wait-between", time.Second*10, "Time to wait between consecutive DNS checks [ACME4NETVS_DNS_WAIT_BETWEEN]")
	rootCmd.PersistentFlags().Duration("dns-wait-after", time.Second*10, "Time to wait after all DNS checks succeed [ACME4NETVS_DNS_WAIT_AFTER]")
}

func FinalizeViperSetup(rootCmd *cobra.Command) {
	// bind viper to commandline flags
	err := viper.BindPFlags(rootCmd.PersistentFlags())
	if err != nil {
		log.Fatal(err)
	}

	// bind settings to environment variables with the same name
	viper.AutomaticEnv()
	viper.SetEnvPrefix("acme4netvs")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))

	// setup logging
	log.SetFlags(log.Lshortfile | log.Ltime)
}

func CmdConfigFromAPIToken(apiToken string, debugPrintf DebugPrintfFunc) (*NETVSConfig, error) {
	var (
		baseURI    string
		apiVersion string
	)
	switch viper.GetString("endpoint") {
	case "prod", "":
		baseURI = BASEURIPROD
	case "test":
		baseURI = BASEURITEST
	case "devel":
		baseURI = BASEURIDEVEL
	default:
		return nil, fmt.Errorf(FormatUnknownEndpoint, viper.GetString("endpoint"))
	}

	// assign variables to circumvent lazy evaluation
	apiVersion = viper.GetString("apiversion")
	apiToken = viper.GetString("apitoken")

	debugPrintf(FormatAPITokenSource, "environment variable or commandline flag")
	return &NETVSConfig{
		BaseURI:      baseURI,
		APIVersion:   apiVersion,
		SessionToken: func() string { return apiToken },
	}, nil
}

func CmdParseNETVSConfig(cfgPath string) (*NETVSConfig, error) {
	var (
		f   *os.File
		err error
	)

	cleanCfgPath := filepath.Clean(cfgPath)
	f, err = os.Open(cleanCfgPath)
	if err != nil {
		return nil, fmt.Errorf(FormatConfigfileCantOpen, cfgPath, err)
	}

	// Explicitly ignore close error as it is not a writable file anyway
	defer func(f *os.File) {
		_ = f.Close()
	}(f)

	// parse config file
	Ini, err := ParseNETVSCredentialsINI(f)
	if err != nil {
		return nil, fmt.Errorf(FormatConfigfileParseError, cfgPath, err)
	}

	return Ini.ToNETVSConfig(viper.GetString("endpoint"), viper.GetString("apiversion")), nil
}

func CmdParseAllNETVSConfigs(debugPrintf DebugPrintfFunc) (*NETVSConfig, error) {
	var (
		netdbConfigPaths []string
		config           *NETVSConfig
		err              error
		homedirpath      string
	)

	// define paths that may contain netdb_client.ini
	if viper.IsSet("config") {
		// try both as path and as file
		netdbConfigPaths = append(netdbConfigPaths, viper.GetString("config"), filepath.Join(viper.GetString("config"), "netdb_client.ini"))
	} else {
		homedirpath, err = homedir.Dir()
		if err == nil {
			netdbConfigPaths = append(netdbConfigPaths, filepath.Join(homedirpath, ".config", "netdb_client.ini"))
		}
		netdbConfigPaths = append(netdbConfigPaths, filepath.Join(".", "netdb_client.ini"))
	}

	// try all config file paths
	for _, cfgPath := range netdbConfigPaths {
		config, err = CmdParseNETVSConfig(cfgPath)
		if err != nil {
			debugPrintf(err.Error())
			continue
		}

		debugPrintf(FormatClientFromIni, cfgPath)
		return config, nil
	}

	// no suitable config file found
	return nil, errors.New(FormatConfigfileNoneFound)
}

func BuildLocalNetVSClient(debugPrintf DebugPrintfFunc) *NETVSClient {
	var err error
	var netvsconfig *NETVSConfig
	if viper.IsSet("apitoken") {
		netvsconfig, err = CmdConfigFromAPIToken(viper.GetString("apitoken"), debugPrintf)
	} else {
		netvsconfig, err = CmdParseAllNETVSConfigs(debugPrintf)
	}
	if err != nil {
		log.Fatal(err)
	}
	debugPrintf(FormatNETVSClientCreated, netvsconfig.BaseURI, netvsconfig.APIVersion)
	return NewNETVSClient(netvsconfig)
}
