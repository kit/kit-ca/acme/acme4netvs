package acme4netvs

import (
	"log"
	"net"
	"strings"
	"sync"
	"time"

	"github.com/spf13/viper"

	"github.com/miekg/dns"
)

const acmeChallengePrefix = `_acme-challenge.`

func HandleChallengePrefix(fqdn string, remove bool, debugPrintf DebugPrintfFunc) string {
	if !strings.HasPrefix(fqdn, acmeChallengePrefix) {
		return fqdn
	}
	if remove {
		strippedFQDN := strings.TrimPrefix(fqdn, acmeChallengePrefix)
		debugPrintf(FormatChallengePrefixRemoved, strippedFQDN)
		return strippedFQDN
	} else {
		debugPrintf(FormatHasChallengePrefix, fqdn)
		return fqdn
	}
}

func NameserversForFQDN(domain string) ([]string, error) {
	// Look for nameservers for domain.
	// Start with the full domain and strip the leftmost part on every iteration until an answer is found.
	labels := dns.SplitDomainName(domain)
	for idx := 0; idx < len(labels); idx++ {
		query := strings.Join(labels[idx:], ".")
		ns, lookupError := net.LookupNS(dns.CanonicalName(query))
		if lookupError == nil && len(ns) > 0 {
			nameserver := make([]string, 0, len(ns))
			for _, host := range ns {
				nameserver = append(nameserver, host.Host)
			}
			return nameserver, nil
		}
	}
	return []string{}, ErrorNoNameserverFound
}

func NameserverHasChallenge(nameserver, fqdn, challenge string) bool {
	var (
		challengeName = buildChallengeFQDN(fqdn)
		client        = new(dns.Client)
	)

	// make sure nameserver contains a port number
	if !strings.Contains(nameserver, ":") {
		nameserver = strings.Join([]string{nameserver, "53"}, ":")
	}

	// build request
	var msg dns.Msg
	msg.SetQuestion(dns.CanonicalName(challengeName), dns.TypeTXT)
	// Set packet size limit as recommended by https://www.dnsflagday.net/2020//#dns-flag-day-2020
	msg.SetEdns0(1232, false)

	// request all TXT records
	TXTAnswer, _, err := client.Exchange(&msg, nameserver)

	// if message is empty/broken/lost or too big, retry over TCP
	if TXTAnswer == nil || TXTAnswer.MsgHdr.Truncated {
		// Set DNS query protocol to TCP to allow for large answers
		client.Net = "tcp"
		// ask DNS about challenge record
		TXTAnswer, _, err = client.Exchange(&msg, nameserver)
	}

	if err != nil {
		return false
	}
	// return true is any TXT record contains the challenge
	for _, answer := range TXTAnswer.Answer {
		if dns.NumField(answer) < 1 {
			continue
		}
		if dns.Field(answer, 1) == challenge {
			return true
		}
	}
	return false
}

func CheckChallengeOnAllNS(
	token string,
	fqdn string,
	globalTimeout time.Duration,
	waitTimeBetweenChecks time.Duration,
	waitTimeAfterChecks time.Duration,
	debugPrintf DebugPrintfFunc) {
	nameservers, err := NameserversForFQDN(fqdn)
	if err != nil {
		log.Fatalf(FormatUnableToFindNS, fqdn, fqdn, err)
	}
	if len(nameservers) == 0 {
		log.Fatalf(FormatNoNSforDomain, fqdn, fqdn)
	}
	debugPrintf(FormatNSList, fqdn, strings.Join(nameservers, ", "))

	var (
		allNameserverDoneWG sync.WaitGroup
		startTime           = time.Now()
	)
	for _, ns := range nameservers {
		allNameserverDoneWG.Add(1)
		go func(nameserver, fqdn, challenge string) {
			var (
				globalTimeoutTicker = time.NewTicker(globalTimeout)
				checkTicker         = time.NewTicker(waitTimeBetweenChecks)
				checkDNS            = make(chan bool)
			)

			defer globalTimeoutTicker.Stop()
			defer checkTicker.Stop()

			// mux initial check and periodic checks
			go func() {
				// initial check
				time.Sleep(time.Millisecond * 500)
				checkDNS <- true
			}()

			// trigger periodic DNS check
			go func() {
				for range checkTicker.C {
					checkDNS <- true
				}
			}()

			defer allNameserverDoneWG.Done()
			debugPrintf(FormatNewGoroutineForFQDN, fqdn, nameserver)
			for {
				select {
				case <-globalTimeoutTicker.C:
					debugPrintf(FormatDNSCheckTimeout, fqdn, nameserver, globalTimeout)
					return
				case <-checkDNS:
					if NameserverHasChallenge(nameserver, fqdn, token) {
						debugPrintf(FormatNSHasChallenge, fqdn, nameserver, time.Since(startTime))
						return
					} else if !viper.GetBool("no-dns-failure-logging") {
						debugPrintf(FormatNSDoesNotHaveChallenge, fqdn, nameserver)
					}
				}
			}
		}(ns, fqdn, token)
	}
	debugPrintf(FormatWaitingForChallenges, len(nameservers))
	allNameserverDoneWG.Wait()
	debugPrintf(FormatDNSChecksDone, fqdn)

	// waiting period after DNS NS checks
	if waitTimeAfterChecks > 0 {
		debugPrintf(FormatDNSWaitAfterChecksWaiting, waitTimeAfterChecks.String())
		time.Sleep(waitTimeAfterChecks)
	}
}
