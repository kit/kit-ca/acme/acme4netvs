//go:generate go run _generators/resolver/main.go
package acme4netvs

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNameserversForFQDN(t *testing.T) {
	tests := []struct {
		Domain     string
		ExpectedNS []string
	}{
		{
			Domain:     "does-not-exist.scc.kit.edu",
			ExpectedNS: KITNameserver,
		},
		// 2022-12-28: this is currently broken within the DNS system.
		// Should stay disabled until the problem understood and fixed.
		// {
		// 	Domain:     "does-not-exist.le.uni-beispiel.de",
		// 	ExpectedNS: UniBeispielNameserver,
		// },
	}
	for _, test := range tests {
		ns, err := NameserversForFQDN(test.Domain)
		if assert.NoError(t, err) {
			sort.Strings(ns)
			assert.EqualValues(t, test.ExpectedNS, ns)
		}
	}
}

func TestNameserverHasChallenge(t *testing.T) {
	tests := []struct {
		Nameservers    []string
		Domain         string
		Challenge      string
		ExpectedResult bool
	}{
		{
			Nameservers:    KITNameserver,
			Domain:         "does-not-exist.scc.kit.edu",
			Challenge:      "XXX",
			ExpectedResult: false,
		},
		{
			Nameservers:    UniBeispielNameserver,
			Domain:         "mock.le.uni-beispiel.de",
			Challenge:      "763cea2239d4c1280bafa1dc6eb9c66a16af2d6cc3472f4a696eaaa23229a818",
			ExpectedResult: true,
		},
	}

	for _, test := range tests {
		for _, ns := range test.Nameservers {
			res := NameserverHasChallenge(ns, test.Domain, test.Challenge)
			assert.EqualValues(t, test.ExpectedResult, res)
		}
	}
}

func NOPDebugPrintfFunc(_ string, _ ...any) {}

func TestHandleChallengePrefix(t *testing.T) {
	tests := []struct {
		before string
		after  string
		remove bool
	}{
		{
			before: "no.prefix.one.kit.edu",
			after:  "no.prefix.one.kit.edu",
			remove: false,
		},
		{
			before: "_acme-challenge.has.prefix.two.kit.edu",
			after:  "_acme-challenge.has.prefix.two.kit.edu",
			remove: false,
		},
		{
			before: "no.prefix.three.kit.edu",
			after:  "no.prefix.three.kit.edu",
			remove: true,
		},
		{
			before: "_acme-challenge.has.prefix.four.kit.edu",
			after:  "has.prefix.four.kit.edu",
			remove: true,
		},
		{
			before: "weird._acme-challenge.has.prefix.five.kit.edu",
			after:  "weird._acme-challenge.has.prefix.five.kit.edu",
			remove: true,
		},
	}

	for _, test := range tests {
		after := HandleChallengePrefix(test.before, test.remove, NOPDebugPrintfFunc)
		assert.EqualValues(t, test.after, after)
	}
}
