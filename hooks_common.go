package acme4netvs

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	"github.com/mitchellh/go-homedir"
)

type ClientConfig struct {
	ConfigDir            string
	ConfigFile           string
	APIToken             string
	Endpoint             string
	Quiet                bool
	WaitForDNS           bool
	DNSWaitTimeout       time.Duration
	DNSTimeBetweenChecks time.Duration
}

// newNETVSClientFromConfig creates a new NETVSClient from a NETVS config file and an endpoint.
// An empty endpoint creates a client that uses the default production endpoint.
func newNETVSClientFromConfig(configfile string, endpoint string) (*NETVSClient, *NETVSCredentialINI, error) {
	// get path to config file; use default if empty
	if configfile == "" {
		configDir, err := homedir.Dir()
		if err != nil {
			return nil, nil, fmt.Errorf("unable to find homedir: %s", err)
		}

		configfile = filepath.Join(configDir, ".config", "netdb_client.ini")
	}

	// read and parse config file
	cleanConfigfile := filepath.Clean(configfile)
	f, err := os.Open(cleanConfigfile)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to open config file »%s«: %s", configfile, err)
	}

	defer func(f *os.File) {
		_ = f.Close()
	}(f)

	Ini, err := ParseNETVSCredentialsINI(f)
	// file is readonly, errors on close can be ignored
	_ = f.Close()
	if err != nil {
		return nil, nil, fmt.Errorf("unable to parse config file: %s", err)
	}

	// build api client
	return NewNETVSClient(Ini.ToNETVSConfig(endpoint, DefaultAPIVersion)), Ini, nil
}
