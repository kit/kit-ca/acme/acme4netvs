package acme4netvs

// prepend »_acme-challenge.« to fqdn
func buildChallengeFQDN(fqdn string) string {
	return "_acme-challenge." + fqdn
}

// surround data string in ASCII double quotes (as mandated by DNS TXT spec)
func quoteTXTData(data string) string {
	return "\"" + data + "\""
}

// GetInformation creates a Transaction to get all information required for challenge response record creation
func GetInformation(ta Transaction, fqdn string, token string) {
	ta.AddEntries(
		// get fqdn info
		TAStatement{
			Name: "dns.fqdn.list",
			Old: TAStatementDataMap{
				"value": fqdn,
			},
		},
		// get acme fqdn
		TAStatement{
			Name: "dns.fqdn.list",
			Old: TAStatementDataMap{
				"value": buildChallengeFQDN(fqdn),
			},
		},
		// get acme record
		TAStatement{
			Name: "dns.record.list",
			Old: TAStatementDataMap{
				"fqdn_list": []string{buildChallengeFQDN(fqdn)},
				"type":      "TXT",
				"data":      quoteTXTData(token),
			},
		},
	)
}

// CreateFQDN creates Transaction for a generic FQDN creation
func CreateFQDN(ta Transaction, fqdn string, fqdnType string, description string) {
	ta.AddEntries(
		TAStatement{
			Name: "dns.fqdn.create",
			New: TAStatementDataMap{
				"type":        fqdnType,
				"value":       fqdn,
				"description": description,
			},
		},
	)
}

// CreateCertificateFQDN creates Transaction which creates the FQDN we want a certificate for
func CreateCertificateFQDN(ta Transaction, fqdn string) {
	CreateFQDN(ta, fqdn, "domain", "Automatically created by acme4netvs")
}

// CreateChallengeFQDN creates Transaction which creates the "_acme-challenge." FQDN
func CreateChallengeFQDN(ta Transaction, fqdn string) {
	CreateFQDN(ta, buildChallengeFQDN(fqdn), "meta", "ACME dns-01 challenge via acme4netvs")
}

// CreateChallengeRecord creates Transaction which creates the "_acme-challenge." TXT record
func CreateChallengeRecord(ta Transaction, fqdn string, token string) {
	ta.AddEntries(
		TAStatement{
			Name: "dns.record.create",
			New: TAStatementDataMap{
				"fqdn": buildChallengeFQDN(fqdn),
				"type": "TXT",
				"data": quoteTXTData(token),
			},
		},
	)
}

// DeleteChallengeRecord creates Transaction which deletes the "_acme-challenge." TXT record
func DeleteChallengeRecord(ta Transaction, fqdn string, token string) {
	ta.AddEntries(
		TAStatement{
			Name: "dns.record.delete",
			Old: TAStatementDataMap{
				"fqdn": buildChallengeFQDN(fqdn),
				"type": "TXT",
				"data": quoteTXTData(token),
			},
		},
	)
}
