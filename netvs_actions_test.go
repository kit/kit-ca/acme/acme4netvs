package acme4netvs

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/h2non/gock.v1"
)

const TESTAPISERVER = BASEURITEST
const TESTAPIVERSION = DefaultAPIVersion

func TestDeployChallenge(t *testing.T) {
	defer gock.Off()

	tests := []struct {
		Client                *NETVSConfig
		MatchRequestBody      string // currently not in use; just for documentation
		ExpectedResponseBody  string
		FQDN, ChallengeToken  string
		Reply                 int
		CallThisFunction      NetVSActionFunc
		ExpectedResponseError error
	}{
		// Test 02: describe me
		{
			Client: &NETVSConfig{
				BaseURI:      TESTAPISERVER,
				APIVersion:   TESTAPIVERSION,
				SessionToken: GetTestTokenEnv("NETDB_API_TOKEN"),
			},
			MatchRequestBody: `[{"name":"dns.fqdn.list","old":{"value":"le.uni-beispiel.de"}},{"name":"dns.fqdn.list","old":{"value":"_acme-challenge.le.uni-beispiel.de"}},{"name":"dns.record.list","old":{"data":"\"TOKEN\"","fqdn_list":["_acme-challenge.le.uni-beispiel.de"],"type":"TXT"}}]
				`,
			ExpectedResponseBody:  `[[], [], []]`,
			FQDN:                  `le.uni-beispiel.de`,
			ChallengeToken:        `TEST_TOKEN_002`,
			Reply:                 200,
			CallThisFunction:      CleanChallenge,
			ExpectedResponseError: nil,
		},
	}

	// create gock responses
	for _, test := range tests {
		gock.New("https://" + TESTAPISERVER).
			Post("/api/" + TESTAPIVERSION + "/wapi/transaction/execute").
			Persist().
			BodyString(test.Client.SessionToken()).
			BodyString(test.ChallengeToken).
			Reply(test.Reply).
			BodyString(test.ExpectedResponseBody)
	}

	// make test queries
	for _, test := range tests {
		// create new api client with internal gock client
		c := NewNETVSClient(test.Client)
		gock.InterceptClient(c.Client)

		err := test.CallThisFunction(c, test.FQDN, test.ChallengeToken, log.Printf)
		assert.IsType(t, test.ExpectedResponseError, err)
	}
}
