package acme4netvs

import (
	"fmt"

	"github.com/go-viper/mapstructure/v2"
)

type UnexpectedNonTerminalTypeError error

type NetVSActionFunc func(client *NETVSClient, fqdn string, token string, debugPrintf DebugPrintfFunc) error

func DeployChallenge(client *NETVSClient, fqdn string, token string, debugPrintf DebugPrintfFunc) error {
	var (
		err            error
		checkTA        = NewTARequestBody()
		changeTA       = NewTARequestBody()
		fqdnInfo       []DnsFqdn
		acmeFqdnInfo   []DnsFqdn
		acmeRecordInfo []DnsRecord
	)

	// collect NETVS information about fqdn
	GetInformation(checkTA, fqdn, token)
	checkRes, err := client.ExecuteTA(checkTA)
	if err != nil {
		return err
	}

	// decode JSON response
	err = mapstructure.Decode(checkRes[0], &fqdnInfo)
	if err != nil {
		return err
	}
	err = mapstructure.Decode(checkRes[1], &acmeFqdnInfo)
	if err != nil {
		return err
	}
	err = mapstructure.Decode(checkRes[2], &acmeRecordInfo)
	if err != nil {
		return err
	}

	// create requested fqdn if not present
	if len(fqdnInfo) == 0 {
		CreateCertificateFQDN(changeTA, fqdn)
		debugPrintf(FormatGenericInfo, fqdn, "Creating domain record")
	} else if !(fqdnInfo[0].Type == "domain" || fqdnInfo[0].Type == "alias") { // fail if fqdn is nonterminal but not of type alias or domain
		return UnexpectedNonTerminalTypeError(fmt.Errorf("FQDN %s is non-terminal but of unexpected type %s", fqdn, fqdnInfo[0].Type))
	}

	// create challenge record if not present
	if len(acmeRecordInfo) == 0 {
		// create challenge FQDN if not present
		if len(acmeFqdnInfo) == 0 {
			CreateChallengeFQDN(changeTA, fqdn)
			debugPrintf(FormatGenericInfo, fqdn, "Creating _acme-challenge domain record")
		}
		// create Challenge Record
		CreateChallengeRecord(changeTA, fqdn, token)
		debugPrintf(FormatNewAcmeTxtInfo, fqdn, fqdn, token)
	}

	_, err = client.ExecuteTA(changeTA)
	if err != nil {
		return err
	}
	return nil
}

func CleanChallenge(client *NETVSClient, fqdn string, token string, debugPrintf DebugPrintfFunc) error {
	var (
		err            error
		checkTA        = NewTARequestBody()
		changeTA       = NewTARequestBody()
		acmeRecordInfo []DnsRecord
	)

	// collect NETVS information about fqdn
	GetInformation(checkTA, fqdn, token)
	checkRes, err := client.ExecuteTA(checkTA)
	if err != nil {
		return err
	}

	// decode JSON response
	err = mapstructure.Decode(checkRes[2], &acmeRecordInfo)
	if err != nil {
		return err
	}

	// delete record if present
	if len(acmeRecordInfo) != 0 {
		DeleteChallengeRecord(changeTA, fqdn, token)
		debugPrintf(FormatGenericInfo, fqdn, "Removing _acme-challenge TXT record")
	}

	_, err = client.ExecuteTA(changeTA)
	if err != nil {
		return err
	}

	return nil
}
