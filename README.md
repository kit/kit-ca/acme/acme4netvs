# ACME client hooks for SCC NETVS

# Releases

Precompiled binaries are released as [GitLab releases](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/releases), a list of permalinks to the newest version is given [here](https://docs.ca.kit.edu/acme4netvs/en/installation/).
If binaries for your platform are not included, feel free to open an [issue](https://gitlab.kit.edu/kit/kit-ca/acme/acme4netvs/-/issues/new) to get them included in future releases.

# Documentation

Documentation is available [on this website](https://docs.ca.kit.edu/acme4netvs/en/) (currently only in English).

## Note on Build Dependencies
In order to build, [NETDB API Generator](https://gitlab.kit.edu/scc-net/netvs/api-generator) is required, which is written in Python. It can be installed (e.g. in a Python venv) using `pip3 install git+https://gitlab.kit.edu/scc-net/netvs/api-generator.git@main#egg=net-api-generator`.
