#!/usr/bin/env bash

ACME4NETVS="/usr/libexec/acme4netvs/dehydrated_netvs_hook"

if [ ! -x "${ACME4NETVS}" ]; then
  echo "Unable to find dehydrated_netvs_hook"
  exit 1
fi

# Usage: add  _acme-challenge.www.domain.com   "XKrxpRBosdIKFzxW_CT3KLZNf6q0HG9i01zxXp5CPBs"
# Used to add txt record
dns_acme4netvs_add() {
  FULLDOMAIN=$1
  CHALLENGE=$2
  # strip _acme-challenge. prefix
  DOMAIN=${FULLDOMAIN/#_acme-challenge./}

  exec "${ACME4NETVS}" deploy_challenge "${DOMAIN}" "ignore" "${CHALLENGE}"
}

# Usage: fulldomain txtvalue
# Used to remove the txt record after validation
dns_acme4netvs_rm() {
  FULLDOMAIN=$1
  CHALLENGE=$2
  # strip _acme-challenge. prefix
  DOMAIN=${FULLDOMAIN/#_acme-challenge./}

  exec "${ACME4NETVS}" clean_challenge "${DOMAIN}" "ignore" "${CHALLENGE}"
}
