verify_ns() {
	dig_response="$(dig +short @$1 TXT _acme-challenge.$2)"
	result=$(echo "$dig_response" | rg -F -- "$3")
	if [ -z "$result" ]; then
		echo -e $'\u274C'" Challenge $3 not in dig response for $1:"
		echo "$dig_response"
	else
		echo -e $'\u2705 Challenge found on '"$1!"
	fi
}

verify() {
	verify_ns ns-t-1.kit-dns.eu "$1" "$2"
	verify_ns ns-t-2.kit-dns.de "$1" "$2"
}

generate_challenge() {
	date | openssl enc -e -a -k challengekey -aes128 2> /dev/null | tr -d "\n"
}

test_certbot() {
	local test_name="$1"
	export CERTBOT_DOMAIN="$2"
	export CERTBOT_VALIDATION=$(generate_challenge)
	local flags="$3"

	if [ -n "$CHALLENGE_OVERRIDE" ]; then
		export CERTBOT_VALIDATION="$CHALLENGE_OVERRIDE"
	fi

	echo
	echo
	echo "Running certbot test"
	echo -e "  Name:\t\t$test_name"
	echo -e "  Domain:\t$CERTBOT_DOMAIN"
	echo -e "  Challenge:\t$CERTBOT_VALIDATION"
	echo

	deploy_command="go run cmd/certbot_netvs_auth_hook/main.go $flags"
	verify_command="verify $CERTBOT_DOMAIN $CERTBOT_VALIDATION"
	clean_command="go run cmd/certbot_netvs_cleanup_hook/main.go $flags"

	if [ -z "$dry_run" ]; then
		echo "Running $deploy_command"
		$deploy_command
		echo
		echo "Running $verify_command"
		$verify_command
		echo
		echo "Running $clean_command"
		$clean_command
	else
		echo "Not running test as this is a dry run."
		echo -e "Would run\n  $deploy_command"
		echo -e "Would run\n  $verify_command"
		echo -e "Would run\n  $clean_command"
	fi

	unset CERTBOT_DOMAIN CERTBOT_VALIDATION
}

test_dehydrated() {
	local test_name="$1"
	local domain="$2"
	local challenge=$(generate_challenge)
	local flags="$3"

	if [ -n "$CHALLENGE_OVERRIDE" ]; then
		local challenge="$CHALLENGE_OVERRIDE"
	fi

	echo
	echo
	echo "Running dehydrated test"
	echo -e "  Name:\t\t$test_name"
	echo -e "  Domain:\t$domain"
	echo -e "  Challenge:\t$challenge"
	echo

	deploy_command="go run cmd/dehydrated_netvs_hook/main.go deploy_challenge $flags $domain ignored $challenge"
	verify_command="verify $domain $challenge"
	clean_command="go run cmd/dehydrated_netvs_hook/main.go clean_challenge $flags $domain ignored $challenge"

	if [ -z "$dry_run" ]; then
		echo "Running $deploy_command"
		$deploy_command
		echo
		echo "Running $verify_command"
		$verify_command
		echo
		echo "Running $clean_command"
		$clean_command
	else
		echo "Not running test as this is a dry run."
		echo -e "Would run\n  $deploy_command"
		echo -e "Would run\n  $verify_command"
		echo -e "Would run\n  $clean_command"
	fi
}

while getopts d flag; do
	case "$flag" in
		d)
			dry_run="1"
			;;
		*)
			echo "Invalid argument"
			exit 1
			;;
	esac
done

# Setup
unset NETDB_API_TOKEN NETVS_API_TOKEN DNS_WAIT DNS_WAIT_BETWEEN DNS_WAIT_TIMEOUT NETDB_ENDPOINT NETVS_ENDPOINT QUIET
domain_to_test=integration-tests.le.uni-beispiel.de
api_token=$(rg "^token = ([^<]*)$" -r '$1' "$HOME/.config/netdb_client.ini")

test_certbot "NETVS Token from default filepath" "$domain_to_test"
test_dehydrated "NETVS Token from default filepath" "$domain_to_test"

export NETDB_API_TOKEN=$api_token
test_certbot "NETVS Token from environment variable" "$domain_to_test"
test_dehydrated "NETVS Token from environment variable" "$domain_to_test"
unset NETDB_API_TOKEN

hook_options="--apitoken $api_token"
test_certbot "NETVS Token from command line option" "$domain_to_test" "$hook_options"
test_dehydrated "NETVS Token from command line option" "$domain_to_test" "$hook_options"

echo -e "[DEFAULT]\nendpoint = prod\n\n[prod]\nbase_url = api.netdb.scc.kit.edu\ntoken = $api_token" > /tmp/netdb_client.ini

export NETVS_CONFIG_FILE="/tmp/netdb_client.ini"
test_certbot "NETVS Token from custom config file location, path given by environment variable" "$domain_to_test"
test_dehydrated "NETVS Token from custom config file location, path given by environment variable" "$domain_to_test"
unset NETVS_CONFIG_FILE

hook_options="--config /tmp/netdb_client.ini"
test_certbot "NETVS Token from custom config file location, path given by command line argument" "$domain_to_test" "$hook_options"
test_dehydrated "NETVS Token from custom config file location, path given by command line argument" "$domain_to_test" "$hook_options"

rm /tmp/netdb_client.ini

export CHALLENGE_OVERRIDE="-$(generate_challenge)"
test_certbot "Challenge starts with dash" "$domain_to_test"
test_dehydrated "Challenge starts with dash" "$domain_to_test"
unset CHALLENGE_OVERRIDE

export CHALLENGE_OVERRIDE="_$(generate_challenge)"
test_certbot "Challenge starts with underscore" "$domain_to_test"
test_dehydrated "Challenge starts with underscore" "$domain_to_test"
unset CHALLENGE_OVERRIDE
