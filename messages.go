package acme4netvs

import "errors"

var (
	ErrorNoNameserverFound = errors.New("unable to find nameserver for domain")
)

const (
	FormatNSHasChallenge            = "✅ [%s] Challenge is available on %s (after %s)"
	FormatExecutionInfo             = "✨ Running hook %s, version %s"
	FormatAPITokenSource            = "✨ Using NETVS API token from %s"
	FormatClientFromIni             = "✨ Using config file %s"
	FormatWaitingForChallenges      = "✨ Waiting for %d parallel nameserver checks"
	FormatNewGoroutineForFQDN       = "✨ [%s] Started new goroutine to check DNS challenge on %s"
	FormatNSList                    = "✨ [%s] has nameservers %s"
	FormatGenericInfo               = "✨ [%s] %s"
	FormatNewAcmeTxtInfo            = "✨ [%s] Creating new TXT record _acme-challenge.%s: %s"
	FormatChallengeCleanedUp        = "👍 [%s] Removed challenge %s"
	FormatDNSChecksDone             = "👍 [%s] Nameserver checks are done"
	FormatDNSWaitAfterChecksWaiting = "✨ Waiting %s after successful DNS checks"
	FormatConfigfileCantOpen        = "👎 Unable to open config file %s, skipping file: %s"
	FormatCommandNotImplemented     = "👎 operation »%s« is not implemented in this plugin"
	FormatEmptyDomain               = "💀 CERTBOT_DOMAIN is empty"
	FormatConfigfileNoneFound       = "💀 Unable to find a suitable config file"
	FormatConfigfileParseError      = "💀 Unable to parse config file %s: %s"
	FormatUnknownEndpoint           = "💀 Unknown endpoint %s"
	FormatMissingCommand            = "💀 Missing command. See --help for details."
	FormatEmptyValidation           = "💀 [%s] CERTBOT_VALIDATION is empty"
	FormatDNSCheckTimeout           = "💀 [%s] DNS check on nameserver »%s« timed out after %s"
	FormatChallengeDeployment       = "💀 [%s] Error deploying challenge. API error: »%s«"
	FormatUnableToFindNS            = "💀 [%s] Error looking up nameserver for domain %s: %s"
	FormatChallengeCleanup          = "💀 [%s] Error removing challenge. API error: »%s«"
	FormatNoNSforDomain             = "💀 [%s] Unable to find any nameserver for domain %s"
	FormatDurationTooSmall          = "💀 Duration value for %s is too small (%s < %s). Did you omit a time unit?"
	FormatNSDoesNotHaveChallenge    = "🔧️ [%s] Challenge is NOT yet available on %s"
	FormatNETVSClientCreated        = "🚀 NETVS client created (baseURI: »%s«, apiVersion: »%s«)"
	FormatHasChallengePrefix        = "⁉️ Domain »%s« has prefix »_acme-challenge.«. Please check the way you're calling acme4netvs."
	FormatChallengePrefixRemoved    = "️💥 Removed prefix »_acme-challenge.« from domain »%s«. Please fix the way you're calling acme4netvs if possible."
)

const (
	CommonTextHelp = `
Options that may be set using environment variables lists the matching variable names in square brackets.

Duration options need unit suffixes (“ns”, “us/µs”, “ms”, “s”, “m”, “h”) and can be compound expressions "like 1h5s44ms".
`
	CommonTextLatestRelease = `
Visit https://www.ca.kit.edu/p/software/acme4netvs to get the latest release.
`
)
