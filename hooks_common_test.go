package acme4netvs

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewNETVSClientFromConfig(t *testing.T) {
	var (
		err error
	)

	// Tests for not existing default config file
	// Set homevar to ignore potentially existing personal netdb_client.ini
	env := []EnvVariable{
		CreateEnvVariable("HOME", "/tmp/this-shouldnt-exist"),
	}
	restoreFunc := ReversibleEnvVariablesToOS(env)
	_, _, err = newNETVSClientFromConfig("", "irrelevant")
	assert.Error(t, err)
	restoreFunc()

	// Tests for not existing config file
	_, _, err = newNETVSClientFromConfig("testdata/notExistingConfig.ini", "notexisting")
	assert.EqualError(t, err, "unable to open config file »testdata/notExistingConfig.ini«: open testdata/notExistingConfig.ini: no such file or directory")

	// Tests for bad config file (not parseable)
	_, _, err = newNETVSClientFromConfig("testdata/config_noIniFormat.ini", "badsection")
	assert.Error(t, err)

	// Tests for valid config file
	_, ini, err := newNETVSClientFromConfig("testdata/config_newStyle.ini", "existing")
	assert.NoError(t, err)
	expectedSection := NETVSCredentialSection{
		BaseURL: "api.uni-beispiel.de",
		Token:   "test-token",
	}
	assert.Equal(t, expectedSection, ini.Sections[ini.Endpoint])
}
