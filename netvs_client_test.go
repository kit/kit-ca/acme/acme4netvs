package acme4netvs

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	fqdnDomain string
)

func setupNetvsClientTest() error {
	var err error

	ta := NewTARequestBody()

	fqdnDomain = "domain-" + sessionID.String() + "." + TestDomain
	Comment := fmt.Sprintf("Unittest für https://gitlab.kit.edu/KIT/KIT-CA/acme/acme4netvs vom %s", time.Now().Format(time.RFC3339))

	ta.AddEntries(
		TAStatement{
			Name: "dns.fqdn.create",
			New: TAStatementDataMap{
				"type":        "domain",
				"value":       fqdnDomain,
				"description": Comment,
			},
		},
		TAStatement{
			Name: "dns.record.create",
			New: TAStatementDataMap{
				"fqdn":                     fqdnDomain,
				"type":                     "A",
				"data":                     "172.21.73.58",
				"target_is_singleton":      false,
				"target_is_reverse_unique": false,
			},
		},
		TAStatement{
			Name: "dns.record.create",
			New: TAStatementDataMap{
				"fqdn":                     fqdnDomain,
				"type":                     "AAAA",
				"data":                     "2a00:1398:4:14::2000:2000",
				"target_is_singleton":      false,
				"target_is_reverse_unique": false,
			},
		},
	)

	_, err = clientTest.ExecuteTA(ta)
	return err
}

func cleanupNetvsClientTest() error {
	var err error

	ta := NewTARequestBody()
	ta.AddEntries(
		TAStatement{
			Name: "dns.record.delete",
			Old: TAStatementDataMap{
				"fqdn": fqdnDomain,
				"type": "A",
				"data": "172.21.73.58",
			},
		},
		TAStatement{
			Name: "dns.record.delete",
			Old: TAStatementDataMap{
				"fqdn": fqdnDomain,
				"type": "AAAA",
				"data": "2a00:1398:4:14::2000:2000",
			},
		},
		// 2021-08-11: this is currently automated, but it won't be in a future API version
		/*		TAStatement{
				Name: "dns.fqdn.delete",
				Old: TAStatementDataMap{
					"value": fqdnDomain,
				},
			},*/
	)

	_, err = clientTest.ExecuteTA(ta)
	return err
}

func TestNewClient(t *testing.T) {
	var (
		config = NETVSConfig{
			BaseURI:      BASEURITEST,
			SessionToken: GetTestTokenEnv("NETDB_API_TOKEN"),
			APIVersion:   DefaultAPIVersion,
		}
		// single transaction requests as building blocks
		TAStatementDNSFqdnListCERT = TAStatement{
			Name: "dns.fqdn.list",
			Old: TAStatementDataMap{
				"value": "cert.kit.edu",
			},
		}
		TAStatementDNSFqdnListDoesNotExists = TAStatement{
			Name: "dns.fqdn.list",
			Old: TAStatementDataMap{
				"value": "does.not.exist",
			},
		}
		TAStatementDNSRecordCreateInvalid = TAStatement{
			Name: "dns.record.create",
			New: TAStatementDataMap{
				"fqdn": "test.example.com",
				"type": "AAAA",
				"data": "2001:db8::feed:dead:beef",
			},
		}

		// array of all test transactions including expected result
		TestTA = []struct {
			TAList         []TransactionEntry
			ExpectError    bool
			ExpectedResult TAResult
		}{
			{
				TAList: []TransactionEntry{
					TAStatementDNSFqdnListCERT,
					TAStatementDNSFqdnListDoesNotExists,
				},
				ExpectError: false,
				ExpectedResult: TAResult{
					[]interface{}{},
					[]interface{}{},
				},
			},
			{
				TAList: []TransactionEntry{
					TAStatementDNSRecordCreateInvalid,
				},
				ExpectError: true,
			},
		}
	)

	// create NETVS client
	var client = NewNETVSClient(&config)

	for _, talist := range TestTA {
		// create ta
		ta := NewTARequestBody()

		// add transactions
		ta.AddEntries(talist.TAList...)

		// execute ta
		result, err := client.ExecuteTA(ta)

		if talist.ExpectError {
			assert.Error(t, err)
		} else {
			assert.NoError(t, err)
			// 2022-08-17: return value to complex and volatile…
			_ = result
			// assert.Equal(t, result, talist.ExpectedResult)
		}
	}
}
