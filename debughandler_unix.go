//go:build unix

package acme4netvs

import (
	"errors"
	"os"
	"os/signal"
	"os/user"
	"syscall"

	"golang.org/x/sys/unix"
)

// InstallDebugHandler installs a debug handler that listens for SIGUSR1 signals and writes the output to a temporary file.
func InstallDebugHandler() (string, chan string, error) {
	var (
		fd  *os.File
		err error
	)
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGUSR1)

	potentialTempDirs := []string{os.TempDir()}

	// get working directory
	wd, err := os.Getwd()
	if err == nil {
		potentialTempDirs = append(potentialTempDirs, wd)
	} else {
		potentialTempDirs = append(potentialTempDirs, ".")
	}
	// get current user
	usr, err := user.Current()
	if err == nil {
		potentialTempDirs = append(potentialTempDirs, usr.HomeDir)
	}

	// try to open the debug output file
	// guid := xid.New()
	for _, tempDirPath := range potentialTempDirs {
		if unix.Access(tempDirPath, unix.W_OK) != nil {
			// fd, err = os.CreateTemp(tempDirPath, "acme4netvs_debug_"+guid.String()+"_*")
			fd, err = os.CreateTemp(tempDirPath, "acme4netvs_debug_*")
			if err != nil {
				continue
			} else {
				break
			}
		}
	}
	if err != nil {
		return "", nil, errors.New("unable to create temporary file for debug output")
	}

	errorStringChan := make(chan string, 512)
	go func(c chan os.Signal, out chan string) {
		defer close(out)
		for range c {
		}
	}(c, errorStringChan)

	return fd.Name(), errorStringChan, nil
}
