package acme4netvs

import (
	"log"
	"os"
	"testing"

	"github.com/google/uuid"
)

const (
	TestDomain = "netvs-unittests.scc.kit.edu"
)

var (
	sessionID  uuid.UUID
	clientTest *NETVSClient
)

func init() {
	var err error

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	clientTest = NewNETVSClient(&NETVSConfig{
		BaseURI:      BASEURITEST,
		SessionToken: GetTestTokenEnv("NETDB_API_TOKEN"),
		APIVersion:   DefaultAPIVersion,
	})

	sessionID, err = uuid.NewRandom()
	if err != nil {
		panic(err)
	}
}

func GetTestTokenEnv(envvar string) func() string {
	return func() string {
		token := os.Getenv(envvar)
		if token == "" {
			log.Panicf("Could not load token. Environment variable '%s' is empty.", envvar)
		}
		return token
	}
}

func TestMain(m *testing.M) {
	var err error
	// prepare test environment
	err = setupNetvsClientTest()
	if err != nil {
		log.Fatal(err)
	}

	// run tests
	ret := m.Run()

	// cleanup test environment
	err = cleanupNetvsClientTest()
	if err != nil {
		log.Fatal(err)
	}

	os.Exit(ret)
}

// EnvVariable represents an operating system environment variable
// that is either unset or has a string value (including the empty string)
type EnvVariable struct {
	name  string
	isSet bool
	value string
}

func (e EnvVariable) Name() string { return e.name }

// SetInOS sets the corresponding environment variable
func (e EnvVariable) SetInOS() error {
	if e.isSet {
		return os.Setenv(e.name, e.value)
	} else {
		return os.Unsetenv(e.name)
	}
}

// CreateUnsetEnvVariable create an EnvVariable that is unset and has no value
func CreateUnsetEnvVariable(name string) EnvVariable {
	return EnvVariable{
		name:  name,
		isSet: false,
		value: "",
	}
}

// CreateEnvVariable creates a set EnvVariable with a value
func CreateEnvVariable(name string, value string) EnvVariable {
	return EnvVariable{
		name:  name,
		isSet: true,
		value: value,
	}
}

// EnvVariableFromEnv retrieves an envvar from the OS and creates a EnvVariable for it
func EnvVariableFromEnv(name string) EnvVariable {
	val, isSet := os.LookupEnv(name)
	if isSet {
		return CreateEnvVariable(name, val)
	} else {
		return CreateUnsetEnvVariable(name)
	}
}

// EnvVariablesToOS sets (and unsets) the given array of EnvVariable in the OS
func EnvVariablesToOS(vars []EnvVariable) {
	for _, e := range vars {
		_ = e.SetInOS()
	}
}

// ReversibleEnvVariablesToOS sets (and unsets) the given array of EnvVariable in the OS
// and returns a function that reverts these settings.
func ReversibleEnvVariablesToOS(vars []EnvVariable) func() {
	var (
		originalEnvs []EnvVariable
	)
	// save original environment and set new one
	for _, e := range vars {
		originalEnvs = append(originalEnvs, EnvVariableFromEnv(e.name))
		_ = e.SetInOS()
	}
	return func() {
		EnvVariablesToOS(originalEnvs)
	}
}
